const puppeteer = require("puppeteer");
const moment = require("moment");
const util = require("util");
const fs = require("fs");

const commodities = [
  {
    "name": "28.5 mm cotton ( COTTONGUJ ) ",
    "required": true
  },
  {
    "name": "29 mm Cotton ( COTTON ) ",
    "required": true
  },
  {
    "name": "Almond ( BADAM ) ",
    "required": true
  },
  {
    "name": "ALUMINIUM ( ALUMINIUM ) ",
    "required": false
  },
  {
    "name": "Aluminium Ingot ( ALUINGMUM ) ",
    "required": false
  },
  {
    "name": "Arabica Coffee ( COFAPAHSN ) ",
    "required": false
  },
  {
    "name": "Arabica Coffee New ( COFIAPAHSN ) ",
    "required": false
  },
  {
    "name": "Bajra ( BAJRA ) ",
    "required": true
  },
  {
    "name": "Bajra ( BAJRAJPR ) ",
    "required": true
  },
  {
    "name": "Barley ( BARLEYJPR ) ",
    "required": true
  },
  {
    "name": "Brent Crude Oil ( BRENTCRUDE ) ",
    "required": true
  },
  {
    "name": "Carbon Credits ( CERNCDEX ) ",
    "required": false
  },
  {
    "name": "Cashew ( CSHW320KLM ) ",
    "required": true
  },
  {
    "name": "Castor Seed ( CASTOR ) ",
    "required": true
  },
  {
    "name": "Castor Seed ( CASTORDSA ) ",
    "required": false
  },
  {
    "name": "Castor Seed ( CASTORSEED ) ",
    "required": false
  },
  {
    "name": "Castor Seed 1 MT ( CASTOR1MT ) ",
    "required": false
  },
  {
    "name": "Castor Seed 2 MT ( CASTOR2MT ) ",
    "required": false
  },
  {
    "name": "Chana ( CHAMPKIDR ) ",
    "required": false
  },
  {
    "name": "Chana ( CHANA ) ",
    "required": false
  },
  {
    "name": "Chana ( CHANAAKL ) ",
    "required": false
  },
  {
    "name": "Chana ( CHARJDBKN ) ",
    "required": false
  },
  {
    "name": "Chana ( CHARJDDEL ) ",
    "required": false
  },
  {
    "name": "Chana 1 MT ( CHANA1MT ) ",
    "required": false
  },
  {
    "name": "Chana 2 MT ( CHANA2MT ) ",
    "required": false
  },
  {
    "name": "Chandi 30 kg Silver ( CHANDIDEL ) ",
    "required": false
  },
  {
    "name": "Chandi 30 kg Silver ( CHANDIINTL ) ",
    "required": false
  },
  {
    "name": "Chili Sanam ( CHLGSMBGTR ) ",
    "required": false
  },
  {
    "name": "Chilli ( CHLL334GTR ) ",
    "required": false
  },
  {
    "name": "Chilli Guntur LCA 334(Pala) New ( CHLGSMBTMP ) ",
    "required": false
  },
  {
    "name": "Chilli Teja ( CHILLI ) ",
    "required": false
  },
  {
    "name": "Coffee Robusta Cherry AB ( COFFEERC ) ",
    "required": false
  },
  {
    "name": "Common Parboiled Rice ( RICCPARDEL ) ",
    "required": false
  },
  {
    "name": "Common Raw Rice ( RICCRAWDEL ) ",
    "required": false
  },
  {
    "name": "Copper Cathode ( COPPER ) ",
    "required": false
  },
  {
    "name": "Coriander ( DHANIYA ) ",
    "required": false
  },
  {
    "name": "Coriander ( DHANIYAGON ) ",
    "required": false
  },
  {
    "name": "Cotton ( COTTONKADI ) ",
    "required": false
  },
  {
    "name": "Cotton J-34 ( COTJ34ABH ) ",
    "required": false
  },
  {
    "name": "Cotton J-34 ( COTJ34BTD ) ",
    "required": false
  },
  {
    "name": "Cotton J-34 ( COTJ34SIR ) ",
    "required": false
  },
  {
    "name": "Cotton Oilseed Cake Fresh Arrival ( COCUDCFRES ) ",
    "required": false
  },
  {
    "name": "Cotton S-6 ( COTS06ABD ) ",
    "required": false
  },
  {
    "name": "Cotton S-6 ( COTS06KDI ) ",
    "required": false
  },
  {
    "name": "Cotton S-6 ( COTS06RJK ) ",
    "required": false
  },
  {
    "name": "Cotton S-6 ( COTS06SRN ) ",
    "required": false
  },
  {
    "name": "Cotton Seed (Industrial Grade) ( COTTONSEED ) ",
    "required": false
  },
  {
    "name": "Cotton Seed Oilcake ( COCUDAKL ) ",
    "required": false
  },
  {
    "name": "Cotton Seed Oilcake ( COCUDCAKL ) ",
    "required": false
  },
  {
    "name": "Cotton Seed Oilcake ( COCUDCKDI ) ",
    "required": false
  },
  {
    "name": "CPO ( CRDPOLCHN ) ",
    "required": false
  },
  {
    "name": "Crude Palm Oil ( CPO ) ",
    "required": false
  },
  {
    "name": "Crude Palm Oil ( CRDPOLKAK ) ",
    "required": false
  },
  {
    "name": "Crude Palm Oil ( CRDPOLKDL ) ",
    "required": false
  },
  {
    "name": "Crude Palm Oil ( CRDPOLKOL ) ",
    "required": false
  },
  {
    "name": "Degummed Soy oil ( SYODEGUM ) ",
    "required": false
  },
  {
    "name": "Electrolytic Copper Cathode ( COPELCMUM ) ",
    "required": false
  },
  {
    "name": "Electrolytic Copper Cathode ( COPELCMUM1 ) ",
    "required": false
  },
  {
    "name": "Furnace Oil ( FURNACEOIL ) ",
    "required": false
  },
  {
    "name": "Gasoline ( GASOLINE ) ",
    "required": false
  },
  {
    "name": "Gold ( GLDPURAHM ) ",
    "required": false
  },
  {
    "name": "Gold ( GLDPURDEL ) ",
    "required": false
  },
  {
    "name": "Gold ( GLDPURINTL ) ",
    "required": false
  },
  {
    "name": "Gold ( GLDPURMUM ) ",
    "required": false
  },
  {
    "name": "Gold ( GOLD ) ",
    "required": false
  },
  {
    "name": "Gold (100 gms) ( GOLD100 ) ",
    "required": false
  },
  {
    "name": "GOLD (100 gms) ( GOLD100AHM ) ",
    "required": false
  },
  {
    "name": "GOLD (100 gms) ( GOLD100MUM ) ",
    "required": false
  },
  {
    "name": "GOLD (100 gms) ( GOLDIND100 ) ",
    "required": false
  },
  {
    "name": "Gold Hedge ( GOLDHEDGE ) ",
    "required": false
  },
  {
    "name": "Gold Hedge (100 gms) ( GOLDH100 ) ",
    "required": false
  },
  {
    "name": "Gold International ( GOLDINTL ) ",
    "required": false
  },
  {
    "name": "Gold KG ( GLDPURMUMK ) ",
    "required": false
  },
  {
    "name": "Grade A Parboiled Rice ( RICAPARDEL ) ",
    "required": false
  },
  {
    "name": "Grade A Raw Rice ( RICARAWDEL ) ",
    "required": false
  },
  {
    "name": "Gram (Chana) ( CHARJDIDR ) ",
    "required": false
  },
  {
    "name": "Groundnut (In Shell) ( GNSHELGON ) ",
    "required": false
  },
  {
    "name": "Groundnut (In Shell) ( GNSHELJAM ) ",
    "required": false
  },
  {
    "name": "Groundnut (In Shell) ( GNSHELJNG ) ",
    "required": false
  },
  {
    "name": "Groundnut (in shell) – Bikaner ( GNSHLBIKR ) ",
    "required": false
  },
  {
    "name": "Groundnut Oil ( GNOEXPRKT ) ",
    "required": false
  },
  {
    "name": "Guar Gum ( GARGUMJDR ) ",
    "required": false
  },
  {
    "name": "Guar Gum ( GUARGUM ) ",
    "required": false
  },
  {
    "name": "Guar Gum 5 MT ( GUARGUM5 ) ",
    "required": false
  },
  {
    "name": "Guar Seed 1 MT ( GUAR1MT ) ",
    "required": false
  },
  {
    "name": "Guar Seed 10 MT ( GUARSEED10 ) ",
    "required": false
  },
  {
    "name": "GUAR SEED 2 MT ( GUAR2MT ) ",
    "required": false
  },
  {
    "name": "Guarseed ( GARSEDBKN ) ",
    "required": false
  },
  {
    "name": "Guarseed ( GARSEDJDR ) ",
    "required": false
  },
  {
    "name": "Guarseed ( GUARSEDBKN ) ",
    "required": false
  },
  {
    "name": "Guarseed ( GUARSEED ) ",
    "required": false
  },
  {
    "name": "Gur ( GURCHKMZR ) ",
    "required": false
  },
  {
    "name": "Gur ( GURCHMUZR ) ",
    "required": false
  },
  {
    "name": "Gur Balti ( GURBLTHPR ) ",
    "required": false
  },
  {
    "name": "Gur Cold ( GURCOLD ) ",
    "required": false
  },
  {
    "name": "Gur Fresh ( GURFRESH ) ",
    "required": false
  },
  {
    "name": "Heating Oil ( HEATINGOIL ) ",
    "required": false
  },
  {
    "name": "Indian 28 mm cotton ( COTI28AKL ) ",
    "required": false
  },
  {
    "name": "Indian 28 mm Cotton ( COTI28KDI ) ",
    "required": false
  },
  {
    "name": "Indian 28 mm Cotton ( COTI28RKT ) ",
    "required": false
  },
  {
    "name": "Indian 28.5 mm cotton ( COTLSCKADI ) ",
    "required": false
  },
  {
    "name": "Indian 31 mm cotton ( COTI31GTR ) ",
    "required": false
  },
  {
    "name": "Indian Medium Staple Cotton ( COTMSCSRSA ) ",
    "required": false
  },
  {
    "name": "Indian Parboiled Rice ( RICPARRPR ) ",
    "required": false
  },
  {
    "name": "Indian Raw Rice ( RICRAWDEL ) ",
    "required": false
  },
  {
    "name": "IPE Brent 12aprRS ( BRT12APRRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 12aprUSD ( BRT12APRUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 13febRS ( BRT13FEBRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 13febUSD ( BRT13FEBUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 14oct USD ( BRT14OCTUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 14octRS ( BRT14OCTRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15decRS ( BRT15DECRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15decUSD ( BRT15DECUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15junRS ( BRT15JUNRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15junUSD ( BRT15JUNUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15novRS ( BRT15NOVRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 15novUSD ( BRT15NOVUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 16janRS ( BRT16JANRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 16janUSD ( BRT16JANUS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 16mayRS ( BRT16MAYRS ) ",
    "required": false
  },
  {
    "name": "IPE Brent 16mayUSD ( BRT16MAYUS ) ",
    "required": false
  },
  {
    "name": "Jeera ( JEERAUNJHA ) ",
    "required": false
  },
  {
    "name": "Jeera New ( JERNEWUNJ ) ",
    "required": false
  },
  {
    "name": "Jute ( JUTBAGDEL ) ",
    "required": false
  },
  {
    "name": "Jute ( JUTBAGKOL ) ",
    "required": false
  },
  {
    "name": "Jute Bags ( JUTB65KOL ) ",
    "required": false
  },
  {
    "name": "Kachhi Ghani Mustard Oil ( KACHIGHANI ) ",
    "required": false
  },
  {
    "name": "Kapas ( KAPAS ) ",
    "required": false
  },
  {
    "name": "Kapas ( KAPASSRNR ) ",
    "required": false
  },
  {
    "name": "Kapas New ( KAPASSRNRT ) ",
    "required": false
  },
  {
    "name": "Lead ( LEAD ) ",
    "required": false
  },
  {
    "name": "Lemon Tur ( TURLMNMUM ) ",
    "required": false
  },
  {
    "name": "Light Sweet Crude Oil ( CRUDEOIL ) ",
    "required": false
  },
  {
    "name": "Linear Low density Polyethylene ( LLBF01MUM ) ",
    "required": false
  },
  {
    "name": "Long Staple Cotton ( COTLSCKDI ) ",
    "required": false
  },
  {
    "name": "Long Staple Cotton ( COTLSCRKT ) ",
    "required": false
  },
  {
    "name": "Maharashtra Lal Tur ( TURMHLAKL ) ",
    "required": false
  },
  {
    "name": "Maharashtra Lal Tur ( TURMHLJGN ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZEKHRIF ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZYRDNG ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZYRKRM ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZYRNBD  ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZYRNZM ) ",
    "required": false
  },
  {
    "name": "Maize ( MAIZYRRTL ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZE ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZEDEL ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZEDNG ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZEJGN ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZEKRM ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZESGL ) ",
    "required": false
  },
  {
    "name": "Maize - Feed/Industrial Grade ( MAIZESOUTH ) ",
    "required": false
  },
  {
    "name": "Maize Kharif ( MAIZEKHRF ) ",
    "required": false
  },
  {
    "name": "Maize Rabi ( MAIZERABI ) ",
    "required": false
  },
  {
    "name": "Masoor (inclusive of all Taxes) ( MSRIDR ) ",
    "required": false
  },
  {
    "name": "Masoor Grain Bold  ( MSRBLDIDR ) ",
    "required": false
  },
  {
    "name": "Medium Staple Cotton ( COTMSCABH ) ",
    "required": false
  },
  {
    "name": "Medium Staple Cotton ( COTMSCABHR ) ",
    "required": false
  },
  {
    "name": "Medium Staple Cotton ( COTMSCSRS ) ",
    "required": false
  },
  {
    "name": "Mega Silver ( SLVPURDELM ) ",
    "required": false
  },
  {
    "name": "Mentha Oil ( MENTHAOIL ) ",
    "required": false
  },
  {
    "name": "Mentha Oil ( MTHOILCHD ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGBHW ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGGZB ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGKOL ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGMGG ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGMUM ) ",
    "required": false
  },
  {
    "name": "Mild Steel Ingots ( STLINGRPR ) ",
    "required": false
  },
  {
    "name": "Mulberry Green Cocoons ( MGCCBBRNM ) ",
    "required": false
  },
  {
    "name": "Mulberry Raw Silk ( RSMCBBBLR ) ",
    "required": false
  },
  {
    "name": "Mustard Seed 2 MT ( RMSEED2MT ) ",
    "required": false
  },
  {
    "name": "Mustardseed ( RMSEED ) ",
    "required": false
  },
  {
    "name": "Mustardseed ( RMSEEDALW ) ",
    "required": false
  },
  {
    "name": "Mustardseed ( RMSEEDDEL ) ",
    "required": false
  },
  {
    "name": "Mustardseed ( RMSEEDJPR ) ",
    "required": false
  },
  {
    "name": "Mustardseed ( RMSEEDSGN ) ",
    "required": false
  },
  {
    "name": "Mustardseed Oil ( RMOEXPALW ) ",
    "required": false
  },
  {
    "name": "Mustardseed Oil ( RMOEXPBHT ) ",
    "required": false
  },
  {
    "name": "Mustardseed Oil ( RMOEXPJPR ) ",
    "required": false
  },
  {
    "name": "Mustardseed Oil ( RMOEXPSGN ) ",
    "required": false
  },
  {
    "name": "Natural Gas ( NATURALGAS ) ",
    "required": false
  },
  {
    "name": "NCDEXAGRI ( NCDEXAGRI ) ",
    "required": false
  },
  {
    "name": "NICKEL ( NICKEL ) ",
    "required": false
  },
  {
    "name": "Nickel Cathode  ( NICCATMUM ) ",
    "required": false
  },
  {
    "name": "Pepper ( PEPPER ) ",
    "required": false
  },
  {
    "name": "Pepper ( PPRMLGKOC ) ",
    "required": false
  },
  {
    "name": "Platinum ( PLATINUM ) ",
    "required": false
  },
  {
    "name": "Polypropylene ( PPIM10MUM ) ",
    "required": false
  },
  {
    "name": "Polyvinyl Chloride ( PVC ) ",
    "required": false
  },
  {
    "name": "Polyvinyl Chloride ( PVC6567MUM ) ",
    "required": false
  },
  {
    "name": "Potato ( POTATO ) ",
    "required": false
  },
  {
    "name": "Potato ( POTFAQDEL ) ",
    "required": false
  },
  {
    "name": "Pusa Basmati Rice ( RICPUSADEL ) ",
    "required": false
  },
  {
    "name": "R M Oil ( RMOEXPGNR ) ",
    "required": false
  },
  {
    "name": "Rapeseed Mustard Seed Oil Cake ( RMCAKE ) ",
    "required": false
  },
  {
    "name": "Rapeseed-Mustard seed oilcake\n ( MSOILCGNR ) ",
    "required": false
  },
  {
    "name": "Raw Jute ( JUTRAWKOL ) ",
    "required": false
  },
  {
    "name": "RBD Palm Olein ( RBDPLNCHN ) ",
    "required": false
  },
  {
    "name": "RBD Palm Olein ( RBDPLNKAK ) ",
    "required": false
  },
  {
    "name": "RBD Palm Olein ( RBDPLNMUM ) ",
    "required": false
  },
  {
    "name": "RBD Palmolein ( RBDPALMOLN ) ",
    "required": false
  },
  {
    "name": "Ref Soya Oil ( SYOREF ) ",
    "required": false
  },
  {
    "name": "Ref Soya Oil ( SYOREFIDR ) ",
    "required": false
  },
  {
    "name": "Ref Soya Oil ( SYOREFMUM ) ",
    "required": false
  },
  {
    "name": "Ref Soya Oil ( SYOREFNGR ) ",
    "required": false
  },
  {
    "name": "Robusta Coffee ( COFRCBKSN ) ",
    "required": false
  },
  {
    "name": "Robusta Coffee New ( COFIRCBKSN ) ",
    "required": false
  },
  {
    "name": "Rubber RSS4 ( RBRRS4KTM ) ",
    "required": false
  },
  {
    "name": "RUBBER_NEW                     ( RBRRS4KOC ) ",
    "required": false
  },
  {
    "name": "Sesame seed ( SESAMESEED ) ",
    "required": false
  },
  {
    "name": "Sesame Seeds ( SESNW98RKT ) ",
    "required": false
  },
  {
    "name": "Shankar Kapas ( SHANKRKPAS ) ",
    "required": false
  },
  {
    "name": "Shankar Kapas ( SNKRKPSGDL ) ",
    "required": false
  },
  {
    "name": "Shankar Kapas ( SNKRKPSKDI ) ",
    "required": false
  },
  {
    "name": "Shankar Kapas ( SNKRKPSRKT ) ",
    "required": false
  },
  {
    "name": "Shankar Kapas ( SNKRKPSVPR ) ",
    "required": false
  },
  {
    "name": "Silver ( SILVER ) ",
    "required": false
  },
  {
    "name": "Silver ( SLVPURABD ) ",
    "required": false
  },
  {
    "name": "Silver ( SLVPURDEL ) ",
    "required": false
  },
  {
    "name": "Silver ( SLVPURINTL ) ",
    "required": false
  },
  {
    "name": "SILVER (5Kg) ( SILVER5DEL ) ",
    "required": false
  },
  {
    "name": "SILVER 5 KGS AHMEDABAD ( SILVER5AHM ) ",
    "required": false
  },
  {
    "name": "Silver Hedge ( SILVERHEDG ) ",
    "required": false
  },
  {
    "name": "Silver Hedge ( SILVRHEDGE ) ",
    "required": false
  },
  {
    "name": "Silver Hedge 5 Kgs ( SILVERH5 ) ",
    "required": false
  },
  {
    "name": "Silver International ( SILVERINTL ) ",
    "required": false
  },
  {
    "name": "Silver Pur Ahemdabad ( SLVPURAHM ) ",
    "required": false
  },
  {
    "name": "Sona 1 KG Gold ( SONA995INT ) ",
    "required": false
  },
  {
    "name": "Sona 1 KG Gold ( SONA995MUM ) ",
    "required": false
  },
  {
    "name": "Soy Bean ( SYBEANAKL ) ",
    "required": false
  },
  {
    "name": "Soy Bean ( SYBEANBHP ) ",
    "required": false
  },
  {
    "name": "Soy Bean ( SYBEANIDR ) ",
    "required": false
  },
  {
    "name": "Soy Bean ( SYBEANKOT ) ",
    "required": false
  },
  {
    "name": "Soy Bean ( SYBEANNGR ) ",
    "required": false
  },
  {
    "name": "Soya Bean New ( SYBEANIDRT ) ",
    "required": false
  },
  {
    "name": "Soyabean ( SYBEANDWS ) ",
    "required": false
  },
  {
    "name": "Soybean 2 MT ( SYBEAN2MT ) ",
    "required": false
  },
  {
    "name": "Soymeal ( SBMEALIDR ) ",
    "required": false
  },
  {
    "name": "Soymeal ( SBMEALKOT ) ",
    "required": false
  },
  {
    "name": "Soymeal ( SBMEALNGR ) ",
    "required": false
  },
  {
    "name": "Sponge Iron ( SPGIRNRPR ) ",
    "required": false
  },
  {
    "name": "STEEL ( STELINGMGG ) ",
    "required": false
  },
  {
    "name": "STEEL ( STELINGMUM ) ",
    "required": false
  },
  {
    "name": "STEEL ( STELINGRPR ) ",
    "required": false
  },
  {
    "name": "Steel Long ( STEELLONG ) ",
    "required": false
  },
  {
    "name": "Steel Long Commercial ( STEELCOMM ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARM ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARM200 ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMBLG ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMDEL ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMERD ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMKOL ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMKPR ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMMZR ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMVIJ ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGARMVIS ) ",
    "required": false
  },
  {
    "name": "Sugar M Grade ( SUGRMKHPS ) ",
    "required": false
  },
  {
    "name": "Sugar S  ( SUGARSCHN ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARS ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARS150 ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSBLG ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSERD ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSKOL  ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSVIJ ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSVIS ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGARSVSH ) ",
    "required": false
  },
  {
    "name": "Sugar S ( SUGRSKHPS ) ",
    "required": false
  },
  {
    "name": "Thermal Coal ( COAL ) ",
    "required": false
  },
  {
    "name": "Thermal Coal ( COALWANI ) ",
    "required": false
  },
  {
    "name": "Traditional Basmati Rice ( RICTBASDEL ) ",
    "required": false
  },
  {
    "name": "Tur Desi ( TURDESAKL ) ",
    "required": false
  },
  {
    "name": "Turmeric ( TMCFGRERD ) ",
    "required": false
  },
  {
    "name": "Turmeric ( TMCFGRNZM ) ",
    "required": false
  },
  {
    "name": "Turmeric ( TMCFGRSGL ) ",
    "required": false
  },
  {
    "name": "Turmeric NEW ( TMCFGRNZMT ) ",
    "required": false
  },
  {
    "name": "Urad ( URAD ) ",
    "required": false
  },
  {
    "name": "Urad ( URMFAQKOL ) ",
    "required": false
  },
  {
    "name": "Urad ( URMFAQMUM ) ",
    "required": false
  },
  {
    "name": "Urad Desi ( URDDESJAL ) ",
    "required": false
  },
  {
    "name": "V 797 Kapas ( KAPV797SRN ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHEAT ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHEATFAQ ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQABD ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQBRL ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQDEL ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQIDR ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQKHN ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQKOT ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQKPR ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQKRN ) ",
    "required": false
  },
  {
    "name": "Wheat ( WHTSMQSJP ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQBRLI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQDELI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQIDRI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQKHNI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQKOTA ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQKPRI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQKRNI ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQRJKT ) ",
    "required": false
  },
  {
    "name": "Wheat New ( WHTSMQSJPI ) ",
    "required": false
  },
  {
    "name": "Yellow Peas ( YLPEASKPR ) ",
    "required": false
  },
  {
    "name": "Yellow Peas ( YPECANKOL ) ",
    "required": false
  },
  {
    "name": "Yellow Peas ( YPECANMUM ) ",
    "required": false
  },
  {
    "name": "Yellow Soybean Meal (Export) ( SBMEXPKDL ) ",
    "required": false
  },
  {
    "name": "ZINC ( ZINC ) ",
    "required": false
  },
  {
    "name": "Zinc Ingot  ( ZININGMUM ) ",
    "required": false
  }
];

const SPOT_PRICES_URL = "https://www.ncdex.com/MarketData/SpotPrice.aspx";
const DETAILS_BUTTON_SELECTOR = "#ctl00_ContentPlaceHolder3_btnBack";
const COMMODITY_SELECT_SELECTOR = "#ctl00_ContentPlaceHolder3_ddlCommodity";
const INTERVAL = 7; // days

async function init () {
  const browser = await puppeteer.launch({
    headless: false
  });

  const page = await browser.newPage();

  await page.goto(SPOT_PRICES_URL);

  // const commodities = await page.evaluate((selector) => {
  //   const options = document.querySelector(selector).options;
  //   const values = [];

  //   for(let i=0; i<options.length; ++i) {
  //     values.push(options[i].value);
  //   }

  //   return values;
  // }, COMMODITY_SELECT_SELECTOR);

  // const data = commodities.map(commodity => {
  //   return {
  //     name: commodity,
  //     required: false
  //   };
  // });

  // fs.appendFile("commdities.json", JSON.stringify({data}), (err) => {
  //   if (err) throw err;

  //   console.log("saved");
  // });

  const requiredData = commodities.filter(commodity => commodity.required);

  const today = moment().format("DD/MM/YYYY");
  const lastWeek = moment().subtract(INTERVAL, "days").format("DD/MM/YYYY");

  const from = process.argv[2] ? process.argv[2] : lastWeek;
  const to = process.argv[3] ? process.argv[3] : today;

  page.evaluate((from, to) => {
    const FROM_DATE_SELECTOR = "#ctl00_ContentPlaceHolder3_dtFromDate_dtFromDate";
    const TO_DATE_SELECTOR = "#ctl00_ContentPlaceHolder3_dtToDate_dtToDate";

    document.querySelector(FROM_DATE_SELECTOR).value = from;
    document.querySelector(TO_DATE_SELECTOR).value = to;
  }, from, to);

  let spotPrices = {};

  for(let index = 0; index < requiredData.length; ++index) {
    await page.select(COMMODITY_SELECT_SELECTOR, requiredData[index].name);
    await page.click(DETAILS_BUTTON_SELECTOR);
    await page.waitFor(2000);

    const data = await page.evaluate((requiredData) => {
      const DATA_TABLE_SELECTOR = "#ctl00_ContentPlaceHolder3_dgSpotPrice";

      let log = [];

      const table = document.querySelector(DATA_TABLE_SELECTOR);

      for(let i = 1; i < requiredData.length; ++i) {
        try {
          const cells = table.rows.item(i).cells;

          for(let j = 0; j < cells.length; ++j) {
            const date = cells[0].innerHTML.trim() + ", " + cells[1].innerHTML.trim();

            log.push({
              date: date,
              price: Number(cells[2].innerHTML.trim())
            });
          }
        } catch (err) {
          console.error(err);
        }
      }

      return log;
    }, requiredData);

    spotPrices[requiredData[index].name] = data;
  }

  console.log(spotPrices);

  fs.writeFile("commodities.json", JSON.stringify(spotPrices), (err) => {
    if(err) throw err;

    console.log("Commodities saved!");
  });

  browser.close();
}

init();
